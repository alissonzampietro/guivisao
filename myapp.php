<?php
require 'vendor/autoload.php';

use Gui\Application;
use Gui\Components\Button;
use Gui\Components\InputText;
use Gui\Components\Label;

$application = new Application();

$application->on('start', function() use ($application) {

     $input = (new InputText())
        ->setLeft(62)
        ->setTop(20)
        ->setWidth(200);

    $button = (new Button())
        ->setLeft(62)
        ->setTop(60)
        ->setWidth(200)
        ->setValue('Gerar mensagem');

    $label = (new Label())->setFontColor('#f00')->setLeft(62)->setTop(100)->setText('Mensagem: ');

    

    $button->on('click', function() use ($label,$input) {
        $label->setText($label->getText() . $input->getValue());
    });
});

$application->run();
